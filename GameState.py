
class GameState():
  def __init__(self):
    self.state = "Menu"
    self.current_level = 1
    self.score = 0

  def set_state(self, new_state):
    self.state = new_state

  def set_score(self, new_score):
    self.score = new_score