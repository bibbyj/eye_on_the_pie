
import pygame
from Settings import *

class Debug(pygame.sprite.Sprite):
  def __init__(self, draw_type):
    super().__init__()
    pygame.mouse.set_visible(False)
    self.surf = pygame.image.load(resource_path("graphics/owl_small.png")).convert_alpha()
    self.rect = self.surf.get_rect(center = (0,0))
    self.pos = vec((0,0))
    self.draw_type = draw_type

  def update(self):
    self._get_position()

  def draw(self, display_surface):
    if self.draw_type == "Owl":
      display_surface.blit(self.surf, self.rect)
    elif self.draw_type == "Lines":
      self._get_position()
      pygame.draw.line(display_surface,'Black',(self.pos.x,0),(self.pos.x,SCREEN_HEIGHT),2)
      pygame.draw.line(display_surface,'Black',(0,self.pos.y),(SCREEN_WIDTH,self.pos.y),2)

  def _get_position(self):
    self.pos.x, self.pos.y = pygame.mouse.get_pos()
    self.rect.center = self.pos

  def _is_collided(self):
    pass