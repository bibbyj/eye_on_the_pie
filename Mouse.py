
import pygame
from Settings import *

class Mouse():
  def __init__(self):
    self.pressed = (False,False,False)
    self.pos = vec(SCREEN_WIDTH/2,SCREEN_HEIGHT/2)

  def set_buttons(self, button_tuple):
    self.pressed = button_tuple

  def set_pos(self, mouse_pos):
    self.pos = mouse_pos

  def get_buttons(self):
    return self.pressed
  
  def get_pos(self):
    return self.pos
  