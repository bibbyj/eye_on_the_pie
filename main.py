
from sys import exit
# import datetime

import pygame
from Settings import *
from GameState import GameState
from Debug import Debug
from Ingredient import Ingredient
from Pie import Pie
from Mouse import Mouse
from LevelInfo import LevelInfo

pygame.mixer.init()
pygame.init()

BACKGROUND_MUSIC = resource_path("eotp_music_looped.mp3")

screen = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))
clock = pygame.time.Clock()

# pygame.mixer.music.load("eotp_music_looped.mp3")
pygame.mixer.music.load(BACKGROUND_MUSIC)
pygame.mixer.music.play(loops=-1)

game_state = GameState()

# debug = Debug("Lines")
mouse = Mouse()

background_surface = pygame.image.load(resource_path("graphics/background.png")).convert()

font = pygame.font.Font(None, 50)
font_small = pygame.font.Font(None, 30)
title_surf = font.render("Eye On The Pie",False,"Black")
title_rect = title_surf.get_rect(center = (256,220))
score_surf = font.render("Score: " + str(game_state.score),False,"Black")
score_rect = score_surf.get_rect(topleft = (20,20))
version_surf = font_small.render("Version: " + str(VERSION),False,"Black")
version_rect = version_surf.get_rect(topleft = (10,10))
menu_surf = font.render("Click to Play",False,"Black")
menu_rect = title_surf.get_rect(center = (256,320))
credit_surf = font.render("Credits",False,"Black")
credit_rect = title_surf.get_rect(center = (256,SCREEN_HEIGHT-180))
author_surf = font_small.render("Game and music created by Joe",False,"Black")
thanks_surf = font_small.render("Thanks to CaniaEast and jordizzle for assets",False,"Black")
author_rect = title_surf.get_rect(topleft = (40,400))
thanks_rect = thanks_surf.get_rect(topleft = (40,450))
success_surf = font.render("Nice Pie!",False,"Black")
success_rect = title_surf.get_rect(topleft = (100,SCREEN_HEIGHT/3))
bad_surf = font.render("That's a Sad Pie",False,"Black")
bad_rect = bad_surf.get_rect(topleft = (100,SCREEN_HEIGHT/3))
replay_surf = font_small.render("Press Space!",False,"Black")
replay_rect = bad_surf.get_rect(topleft = (100,SCREEN_HEIGHT-300))
fillings_surf = font_small.render("Fillings: " + str("Cherry"),False,"Black")
fillings_rect = fillings_surf.get_rect(topleft = (100,100))

level_info = LevelInfo()
pie = Pie()

while True:

  mouse.set_buttons((False,False,False))
  mouse.set_pos(pygame.mouse.get_pos())

  for event in pygame.event.get():

    # Look for quit
    if event.type == pygame.QUIT:
      pygame.mixer.music.stop()
      pygame.quit()
      exit()

    # Look for escape key
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_ESCAPE:
        pygame.mixer.music.stop()
        pygame.quit()
        exit()
      elif event.key == pygame.K_SPACE:
        if game_state.state == "Results":
          if game_state.score < 5:
            game_state.set_score(0)
            game_state.set_state("Game")
          else:
            if game_state.current_level < 3:
              game_state.current_level += 1
              game_state.set_score(0)
              game_state.set_state("Game")
            else:
              game_state.set_state("Menu")              


    # update mouse status for following sections
    if event.type == pygame.MOUSEBUTTONDOWN:
      mouse.set_buttons(pygame.mouse.get_pressed())
        

  if game_state.state == "Menu":
    pygame.mouse.set_visible(True)

    screen.fill((50,50,50))
    screen.blit(background_surface,(0,0))
    screen.blit(title_surf, title_rect)
    screen.blit(menu_surf, menu_rect)
    screen.blit(credit_surf, credit_rect)
    screen.blit(version_surf, version_rect)

    # debug.update()
    # debug.draw(screen)

    for button in mouse.get_buttons():
      if button:
        if credit_rect.collidepoint(mouse.get_pos()):
          game_state.set_state("Credits")
        else:        
          game_state.current_level = 1
          game_state.set_score(0)
          game_state.set_state("Game")
          scoring_ingredients = level_info.get_pie_ingredients(game_state.current_level)
          nonscoring_ingredients = level_info.get_other_ingredients(game_state.current_level)
          ig1 = Ingredient(scoring_ingredients, True)
          ig2 = Ingredient(nonscoring_ingredients, False)
          ig3 = Ingredient(nonscoring_ingredients, False)
          ig4 = Ingredient(nonscoring_ingredients, False)
          ig5 = Ingredient(nonscoring_ingredients, False)
          
  elif game_state.state == "Results":
    ig1.consumed()
    ig2.consumed()
    ig3.consumed()
    ig4.consumed()
    ig5.consumed()
          
    if game_state.score >= 5:
      # display good pie, increment level
      screen.fill((50,50,50))
      screen.blit(background_surface,(0,0))
      screen.blit(success_surf, success_rect)
      screen.blit(replay_surf, replay_rect)
      pie.fill(5)
      pie.draw(screen)
      next_level = game_state.current_level + 1
      scoring_ingredients = level_info.get_pie_ingredients(next_level)
      nonscoring_ingredients = level_info.get_other_ingredients(next_level)
      ig1.new_pie(scoring_ingredients, True)
      ig2.new_pie(nonscoring_ingredients, False)
      ig3.new_pie(nonscoring_ingredients, False)
      ig4.new_pie(nonscoring_ingredients, False)
      ig5.new_pie(nonscoring_ingredients, False)
      
    else:
      # display wrong pie, display try again
      screen.fill((50,50,50))
      screen.blit(background_surface,(0,0))
      screen.blit(bad_surf, bad_rect)
      screen.blit(replay_surf, replay_rect)
      pie.fill(0)
      pie.draw(screen)      
          
  elif game_state.state == "Credits":
    pygame.mouse.set_visible(True)
    # debug.update()

    screen.fill((50,50,50))
    screen.blit(background_surface,(0,0))
    screen.blit(title_surf, title_rect)
    screen.blit(author_surf, author_rect)
    screen.blit(thanks_surf, thanks_rect)

    # debug.draw(screen)
    
    for button in mouse.get_buttons():
      if button:
        game_state.set_state("Menu")

  elif game_state.state == "Game":
    pygame.mouse.set_visible(False)
    fillings_list = ""
    fillings_list = fillings_list.join([ig+" " for ig in scoring_ingredients])
    fillings_surf = font_small.render("Fillings: " + fillings_list,False,"Black")
    
    # update positions etc
    # debug.update()
    ig1.update()
    ig2.update()
    ig3.update()
    ig4.update()
    ig5.update()
    pie.update()

    # collision checks
    if pie.rect.colliderect(ig1):
      ig1.consumed()
      game_state.set_score(game_state.score + 1)
    if pie.rect.colliderect(ig2):
      game_state.set_state("Results")
    if pie.rect.colliderect(ig3):
      game_state.set_state("Results")
    if pie.rect.colliderect(ig4):
      game_state.set_state("Results")
    if pie.rect.colliderect(ig5):
      game_state.set_state("Results")

    # blit to screen
    score_surf = font.render("Score: " + str(game_state.score),False,"Black")
    screen.fill((50,50,50))
    screen.blit(background_surface, (0,0))
    # screen.blit(score_surf, score_rect)
    screen.blit(fillings_surf, fillings_rect)
    
    pie.fill(game_state.score)

    ig1.draw(screen)
    ig2.draw(screen)
    ig3.draw(screen)
    ig4.draw(screen)
    ig5.draw(screen)
    pie.draw(screen)
    # debug.draw(screen)
    
    if game_state.score >= 5:
      game_state.set_state("Results")

  ## ---------------------- 

  # update screen!
  pygame.display.update()
  clock.tick(FPS)