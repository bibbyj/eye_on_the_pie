import pygame
from Settings import *

class Pie(pygame.sprite.Sprite):
  def __init__(self):
    super().__init__()
    # self.surf = pygame.Surface((160,40))
    self.fill_image = "pie_squashed.png"
    self.surf = pygame.image.load(resource_path("graphics/pie_squashed.png")).convert()
    self.surf.set_colorkey((255,255,255), pygame.RLEACCEL)
    self.rect = self.surf.get_rect(center = (400,400))
    self.pos = vec(400,400)

  def update(self):
    self._get_position()
    self.rect.midbottom = (self.pos.x,SCREEN_HEIGHT-30)

  def draw(self, display_surface):
    display_surface.blit(self.surf, self.rect)
    
  def fill(self, score):
    fill_images = {
      0: "pie_squashed.png", 
      1: "pie_1.png",
      2: "pie_2.png",
      3: "pie_3.png",
      4: "pie_4.png",
      5: "pie_full.png",
      }
    self.fill_image = fill_images.get(score, "pie_squashed.png")
    self.surf = pygame.image.load(resource_path("graphics/" + self.fill_image)).convert()
    self.surf.set_colorkey((255,255,255), pygame.RLEACCEL)    

  def _get_position(self):
    self.pos.x, self.pos.y = pygame.mouse.get_pos()

