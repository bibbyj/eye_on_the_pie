# Eye On The Pie

## Playing the Game

To make it easy, I've packaged the game for Windows over on itch.io:
https://thedailywaffle.itch.io/eye-on-the-pie

To run the game natively with Python, run main.py

You'll need to have the pygame library installed. Release 1.0 uses pygame 2.5.2 with Python 3.9.2

## Notes

- Looks like I need a class for the text in the game
- Utilise sprite groups to allow looping over the ingredients
- Potential to move out the inner workings of Game/Credits/Results etc to their own heirarchy
- Find a standard way to detect mouse button down that works with the general outline I've already estabilished (don't put too much in the main pygame event loop)
- LevelInfo needs rework so that the extra level doesnt need to be added to the dicts
- There's a few hardcoded numbers that can come out, probably into the Settings file

## Contributions

I'd like to thank CaniaEast and jordizzle for assets:

- Background by CaniaEast   https://caniaeast.itch.io/simple-sky-pixel-backgrounds
- Fruits by jordizzle       https://jordizzle.itch.io/farming-game-asset-pack-1
