
class LevelInfo():
  def __init__(self):
    self.pie_ingredients = {
      1: ["Cherry"],
      2: ["Apple", "Ginger"],
      3: ["Blueberries", "Raspberries"],
      4: ["Banana"] #lazy
    }
    self.other_ingredients = {
      1: ["Peach",
          "Raspberries",
          "Apple",
          "Ginger",
          "Jalapeno"],
      2: ["Peach",
          "Raspberries",
          "Jalapeno",
          "Banana",
          "Spinach"],
      3: ["Cherry",
          "Cranberries",
          "Grapes",
          "Strawberries",
          "WildBerries"],
      4: ["Spinach"] #lazy
    }
  
  def get_pie_ingredients(self, level):
    if level == 0:
      print("ERROR")
      return []
    else:
      return self.pie_ingredients.get(level)
    
  def get_other_ingredients(self, level):
    if level == 0:
      print("ERROR")
      return []
    else:
      return self.other_ingredients.get(level)