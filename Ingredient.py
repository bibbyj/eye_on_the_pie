
import pygame
from Settings import *
from random import choice, randint

class Ingredient(pygame.sprite.Sprite):
  def __init__(self, ingredient_list, scoring):
    super().__init__()
    self.items = ingredient_list
    self.scoring = scoring
    self._reset()
    self.rect = self.surf.get_rect()

  def _reset(self):
    self.surf = pygame.image.load(resource_path("graphics/" + choice(self.items) + ".png")).convert_alpha()
    self.pos = vec(randint(8,SCREEN_WIDTH-8),randint(-200,0))

    if self.scoring:
      self.speed = randint(3,5)
    else:
      self.speed = randint(4,6)

  def update(self):
    self.pos.y += self.speed
    self.rect.midbottom = self.pos

    if self.pos.y > SCREEN_HEIGHT:
      self._reset()

  def draw(self, display_surface):
    display_surface.blit(self.surf, self.rect)

  def consumed(self):
    self._reset()
    
  def new_pie(self, ingredient_list, scoring):
    self.items = ingredient_list
    self.scoring = scoring
    self._reset()
    
    

