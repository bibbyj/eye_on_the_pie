
import pygame
import os
import sys

SCREEN_WIDTH = 512
SCREEN_HEIGHT= 768
FPS = 60

vec = pygame.math.Vector2

VERSION = 1.0

def resource_path(relative_path):
     if hasattr(sys, '_MEIPASS'):
         return os.path.join(sys._MEIPASS, relative_path)
     return os.path.join(os.path.abspath("."), relative_path) 